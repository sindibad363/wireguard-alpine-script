#!/bin/sh
__first() {
    clear
    echo "Wireguard Alpine Script"
    echo " i) Install"
    echo " c) Configuration"
    echo " e) Exit"
    read -r -n1 -p "What would you like to do?" __startup
    case $__startup in
    "i"|"I")
    __install ;;
    "e"|"E")
    exit 0;;
    "c"|"C")
    __second ;;
    *)
    __first ;;
    esac
}
__second(){
    clear
    echo "Wireguard-Alpine-VPN"
    echo " a) Add a user"
    echo " d) Delete a user"
    echo " l) List users"
    echo " p) Print QR-Code"
    echo " P) Print Config"
    echo " e) Edit User Config"
    echo ""
    echo " E) Edit Server Config"
    echo " B) Backup"
    echo " Q) Quit"
    read -r -n1 -p "What would you like to do?" __read
    case $__read in
    "a")
    __add_user;;
    "d")
    __del_user;;
    "l")
    __list_users;;
    "p")
    __print_config;;
    "P")
    __print_config_qr;;
    "e")
    __edit_user;;
    "E")
    __edit_server;;
    "B")
    __backup;;
    "Q")
    exit 0;;
    *)
    __second;;
    esac

}

__add_user() {
    if [ ! -d "/etc/wireguard/configs" ];then mkdir /etc/wireguard/configs;fi
    echo "" && read -p "Enter username: " __username
    if cd /etc/wireguard/keys; then if [ ! -f "${__username}_private_key" ]; then wg genkey | tee ${__username}_private_key | wg pubkey > ${__username}_public_key; fi;fi
    ips=($(cat /etc/wireguard/config | grep 'UsedIPs' | sed 's/^.*= //'))
    newip=$(for i in {0..255};do if [[ -z $(echo ${ips[$i]} | cut -d"." -f4) ]]; then echo "$((${i}+1))/24";break;fi; done)
    newip="$(cat /etc/wireguard/config | grep 'AddressAllocation' | sed 's/^.*= //' | cut -d "." -f1-3).${newip}"
    echo -e "[Interface]\nPrivateKey = $(cat /etc/wireguard/keys/${__username}_private_key)\nAddress = $newip\n\n[Peer]\nPublicKey = $(cat /etc/wireguard/keys/server_public_key)\nEndpoint = $(cat /etc/wireguard/config | grep 'ServerIP' | sed 's/^.*= //' )\nAllowedIPs = $(cat /etc/wireguard/config | grep 'AddressAllocation' | sed 's/^.*= //' )" > /etc/wireguard/configs/${__username}.conf
    echo -e "#${__username}\n[Peer]\nPublicKey = $(cat /etc/wireguard/keys/${__username}_public_key)\nAllowedIPs = ${newip}\n#${__username} end" >> /etc/wireguard/wg0.conf
    wg-quick down wg0
    wg-quick up wg0
}
__install() {
    clear
    read -n1 -p "Enable Community Repo(Y/n) " __test
    if [ "$__test" = "y" ];then
        if [ "$(cat /etc/apk/repositories | grep "http://dl-cdn.alpinelinux.org/alpine/v$(cat /etc/os-release | grep PRETTY | sed 's/^.*\(v\)//' | sed 's/"//')/community")" = "#http://dl-cdn.alpinelinux.org/alpine/v$(cat /etc/os-release | grep PRETTY | sed 's/^.*\(v\)//' | sed 's/"//')/community" ]; then
        cp /etc/apk/repositories /etc/apk/repositories.bak
        echo "http://dl-cdn.alpinelinux.org/alpine/v$(cat /etc/os-release | grep PRETTY | sed 's/^.*\(v\)//' | sed 's/"//')/community" >> /etc/apk/repositories
        else
        echo -e "\nCommunity Repo already enabled \n"
        fi
        apk update
    fi
    if [ -z "$(apk version curl | grep curl)" ];then apk add curl;else echo -e "\ncurl installed, skipping installation of package";fi
    if [ -z "$(apk version wireguard-tools | grep wireguard-tools)" ];then apk add wireguard-tools;else echo -e "\nwireguard-tools installed, skipping installation of package";fi
    if [ -z "$(apk version libqrencode | grep libqrencode)" ];then apk add libqrencode;else echo -e "\nlibqrencode installed, skipping installation of package";fi
    if [ -z "$(apk version awall | grep awall)" ];then apk add iptables ip6tables awall;else echo -e "\nawall installed, skipping installation of package";fi
    if cd /etc/wireguard; then
        umask 077
        if [ ! -d "/etc/wireguard/keys" ];then mkdir /etc/wireguard/keys;fi
        if cd /etc/wireguard/keys; then wg genkey | tee server_private_key | wg pubkey > server_public_key; fi
        if [ ! -d "/etc/wireguard/configs" ];then mkdir /etc/wireguard/configs;fi
        read -p "Server IP (External {Can also be DNS Record}) [$(curl -s ifconfig.me)] " __ip
        __ip=${__ip:-$(curl -s ifconfig.me)}
        read -p "Wireguard Port [51820]: " __port
        __port=${__port:-51820}
        read -p "Wireguard Address Allocation [10.8.0.0/24]: " __adress_aloc
        __adress_aloc=${__adress_aloc:-10.8.0.0/24}
        echo -e "Port= $__port\nServerIP= $__ip\nAddressAllocation= $__adress_aloc\nServerPub= $(cat /etc/wireguard/keys/server_public_key)\nUsedIPs= " > /etc/wireguard/config
        echo -e "[Interface]\nListenPort = $__port\nPrivateKey = $(cat /etc/wireguard/keys/server_private_key)\n\n" > /etc/wireguard/wg0.conf
    fi

    #AWall
    #if [ ! -f "/etc/awall/private/wireguard.json" ];then
    #    modprobe -v ip_tables
    #    rc-update add iptables
    #    echo -e "{\n    "service": {\n        "wireguard": [\n\n            { "proto": "udp", "port": 31194 }\n        ]\n    }\n}" > /etc/awall/private/custom_services.json
    #    echo -e "{\n  "description": "Default awall policy to protect Cloud server",\n  "import": "custom-services",\n  "variable": { "internet_if": "eth0"},\n  "zone": {\n    "internet": { "iface": "$internet_if" },\n    "vpn": { "iface": "wg0" }\n  },\n  "policy": [\n    { "in": "internet", "action": "drop" },\n    { "in": "vpn", "out": "internet", "action": "accept" },\n    { "out": "vpn", "in": "internet", "action": "accept" },\n    { "action": "reject" }\n  ],\n  "snat": [ { "out": "internet", "src": "$(cat /etc/wireguard/config | grep Alloc | sed 's/^.*\( \)//')" } ]\n}" > /etc/awall/optional/cloud-server.json
    #    
    #    awall enable wireguard
    #    awall activate
    __second
}
if [ -d "/etc/wireguard" ];then
    __second
else
    __first
fi
